export enum CustomFavicon {
  FAVICON_ALFAONE = 'faviconAlfaOne',
  FAVICON_GROUP9 = 'faviconGroup9',
  FAVICON_HAYSTAQ = 'faviconHaystaq',
  FAVICON_MSTACK = 'faviconMStack',
  FAVICON_BRIGHTR = 'faviconBrightr',
  FAVICON_MERAPAR = 'faviconMerapar',
}

export type AvailableCustomFavicons = { [key in CustomFavicon]: string };

export const customFavicons: AvailableCustomFavicons = {
  faviconAlfaOne: 'icons/favicon-alfaone.png',
  faviconGroup9: 'icons/favicon-group9.png',
  faviconHaystaq: 'icons/favicon-haystaq.png',
  faviconMStack: 'icons/favicon-mstack.png',
  faviconBrightr: 'icons/favicon-brightr.png',
  faviconMerapar: 'icons/favicon-merapar.png',
};

export interface CompanyConfiguration {
  name: string,
  title: string,
  favicon: CustomFavicon
}

export const companyMapping : { [keycloakToken: string]: CompanyConfiguration } = {
  'alfaone': {
    name: 'AlfaOne',
    title: 'AlfaOne',
    favicon: CustomFavicon.FAVICON_ALFAONE
  },
  'group9': {
    name: 'GROUP9 B.V.',
    'title': 'GROUP9 B.V.',
    favicon: CustomFavicon.FAVICON_GROUP9
  },
  'haystaq': {
    name: 'Haystaq',
    'title': 'Haystaq',
    favicon: CustomFavicon.FAVICON_HAYSTAQ
  },
  'mstack': {
    name: 'mStack',
    'title': 'mStack',
    favicon: CustomFavicon.FAVICON_MSTACK
  },
  'brighr': {
    name: 'bright-r',
    'title': 'bright-r',
    favicon: CustomFavicon.FAVICON_BRIGHTR
  },
  'merapar': {
    name: 'Merapar',
    'title': 'Merapar',
    favicon: CustomFavicon.FAVICON_MERAPAR
  },
}
