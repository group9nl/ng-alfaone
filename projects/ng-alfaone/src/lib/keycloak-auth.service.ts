import { Injectable } from '@angular/core';
import {KeycloakService} from 'keycloak-angular';

@Injectable({
  providedIn: 'root'
})
export class KeycloakAuthService {

  private authSuccessCallbacks: Array<{(keycloak: KeycloakService, isLoggedIn: boolean): void }> = [];

  constructor(private readonly keycloak: KeycloakService) {
    keycloak.getKeycloakInstance().onAuthSuccess = () => {
      this.authSuccessCallbacks.forEach(fn => {
        this.keycloak.isLoggedIn().then((isLoggedIn) => fn(this.keycloak, isLoggedIn));
      });
    }
  }

  registerAuthSuccessCallback(callback: {(keycloak: KeycloakService, isLoggedIn: boolean): void}) {
    this.authSuccessCallbacks.push(callback);
    this.keycloak.isLoggedIn().then((isLoggedIn) => callback(this.keycloak, isLoggedIn));
  }
}
