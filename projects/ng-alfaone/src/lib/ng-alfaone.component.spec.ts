import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgAlfaoneComponent } from './ng-alfaone.component';

describe('NgAlfaoneComponent', () => {
  let component: NgAlfaoneComponent;
  let fixture: ComponentFixture<NgAlfaoneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NgAlfaoneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgAlfaoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
