import {Component, Input, OnInit} from '@angular/core';
import {NgxFaviconService} from 'ngx-favicon';
import {NgAlfaoneService} from './ng-alfaone.service';
import {CustomFavicon} from './favicons';

@Component({
  selector: 'ng-alfaone',
  template: '',
  styles: [
  ]
})
export class NgAlfaoneComponent implements OnInit {

  static faviconId = 'favicon';

  @Input()
  title: string = "";

  constructor(private alfaOneService: NgAlfaoneService,
              private ngxFaviconService: NgxFaviconService<CustomFavicon>,
              ) {
    alfaOneService.configuration().subscribe(config => {
      console.log(`Setting config to ${config.favicon}`)
      this.ngxFaviconService.setCustomFavicon(config.favicon);
    })

  }

  ngOnInit(): void {

  }

}

