import { NgModule } from '@angular/core';
import { NgAlfaoneComponent } from './ng-alfaone.component';
import {NgxFaviconModule} from 'ngx-favicon';
import {customFavicons} from './favicons';
import {NgAlfaoneService} from './ng-alfaone.service';
import {KeycloakAuthService} from './keycloak-auth.service';

@NgModule({
  providers: [KeycloakAuthService, NgAlfaoneService],
  declarations: [NgAlfaoneComponent],
  imports: [
    NgxFaviconModule.forRoot({
      faviconElementId: NgAlfaoneComponent.faviconId,
      defaultUrl: customFavicons.faviconAlfaOne,
      custom: customFavicons
    })
  ],
  exports: [NgAlfaoneComponent]
})
export class NgAlfaoneModule { }
