import { TestBed } from '@angular/core/testing';

import { NgAlfaoneService } from './ng-alfaone.service';

describe('NgAlfaoneService', () => {
  let service: NgAlfaoneService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NgAlfaoneService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
