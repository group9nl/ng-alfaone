import {Inject, Injectable, Input} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {CompanyConfiguration, companyMapping} from './favicons';
import {KeycloakService} from 'keycloak-angular';
import {KeycloakTokenParsed} from 'keycloak-js';
import {KeycloakAuthService} from './keycloak-auth.service';

@Injectable({
  providedIn: 'root'
})
export class NgAlfaoneService {

  static defaultCompany = 'alfaone';

  private _configuration = new BehaviorSubject<CompanyConfiguration>(NgAlfaoneService.config(NgAlfaoneService.defaultCompany));

  constructor(private readonly keycloak: KeycloakAuthService) {
    this.keycloak.registerAuthSuccessCallback((keycloak: KeycloakService) => {
      const tokenParsed = keycloak.getKeycloakInstance().tokenParsed as AlfaOneKeycloakTokenParsed
      const company = tokenParsed.company.toLowerCase();
      this._configuration.next(NgAlfaoneService.config(company));
    });
  }

  configuration(): Observable<CompanyConfiguration> {
    return this._configuration.asObservable();
  }

  private static config(company: string): CompanyConfiguration {
    return companyMapping[company] || companyMapping[NgAlfaoneService.defaultCompany];
  }
}

interface AlfaOneKeycloakTokenParsed extends KeycloakTokenParsed {
  company: string;
}
